#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "RenderWindow.hpp"

class AssetManager
{
public:
	static SDL_Texture* GetCharacterTexture(RenderWindow window)
	{
		return window.LoadTexture("res/imgs/character.png");
	}

	static SDL_Texture* GetGroundTexture(RenderWindow window)
	{
		return window.LoadTexture("res/imgs/ground.png");
	}

	static SDL_Texture* GetEnemyTexture(RenderWindow window)
	{
		return window.LoadTexture("res/imgs/enemy.png");
	}

private:
	AssetManager(){};
};