#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "Math.hpp"
#include "PhysicsData.hpp"

class Entity
{
public:
	Entity(Vector2f p_v, SDL_Texture* p_tex, SDL_Event* p_event, float width, float height);
	void MoveEntity(Vector2f p_dir);
	SDL_Texture* tex;
	Vector2f position;
	virtual void Update();
	SDL_Event* event;
	PhysicsData physicsData;

private:
	SDL_Rect currentFrame;
};