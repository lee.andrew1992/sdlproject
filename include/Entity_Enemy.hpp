#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <vector>
#include <memory>
#include "Entity.hpp"
#include "Entity_Player.hpp"
#include "RenderWindow.hpp"
#include "Math.hpp"

class Entity_Enemy: public Entity
{
public:
	Entity_Enemy(Vector2f p_v, SDL_Texture* texture, SDL_Event* event, float width, float height, RenderWindow window): Entity(p_v, texture, event, width, height)
	{
		screenWidth = window.screenWidth;
		screenHeight = window.screenHeight;
	}

	void Update() override;

private:
	float screenWidth;
	float screenHeight;
};