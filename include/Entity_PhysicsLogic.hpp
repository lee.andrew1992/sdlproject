#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <vector>
#include <memory>
#include "Entity.hpp"
#include "Entity_Player.hpp"
#include "Entity_Enemy.hpp"

class Entity_PhysicsLogic
{
public:
	static bool GroundCheck(std::vector<std::shared_ptr<Entity>> groundEntities, std::shared_ptr<Entity_Player> player);
	static bool DeathCheck(float windowHeight, std::vector<std::shared_ptr<Entity_Enemy>> enemyEntities, std::shared_ptr<Entity_Player> player);

	template <typename EntityType> static int EnemyCheck(std::shared_ptr<Entity_Enemy> enemyEntity, std::shared_ptr<EntityType> entity)
	{
		if (enemyEntity->position.x + enemyEntity->physicsData.width < entity->position.x)
		{
			return false;
		}

		if ((entity->position.x >= enemyEntity->position.x && entity->position.x < enemyEntity->position.x + enemyEntity->physicsData.width) ||
			(entity->position.x + entity->physicsData.width >= enemyEntity->position.x && entity->position.x + entity->physicsData.width < enemyEntity->position.x + enemyEntity->physicsData.width))
		{
				if (entity->position.y + entity->physicsData.height >= enemyEntity->position.y &&
					entity->position.y + entity->physicsData.height <= enemyEntity->position.y + enemyEntity->physicsData.height)
				{
					return true;
				}
		}

		return false;
	};

	static void ProjectileHitCheck(std::vector<std::shared_ptr<Entity_Enemy>> &enemyEntities, std::vector<std::shared_ptr<Entity_Projectile>> &projectiles);

private:
	Entity_PhysicsLogic();
};