#pragma once
#include <vector>
#include <memory>
#include "Entity_Projectile.hpp"
#include "Entity.hpp"
#include "Math.hpp"

class Entity_Player: public Entity
{
public:
	Entity_Player(Vector2f p_v, SDL_Texture* texture, SDL_Event* event, float width, float height): Entity(p_v, texture, event, width, height) {
		gravity = 0.5;
		jumpStrength = 2;
		maxJumpHeight = 400;
		isGrounded = false;
		currentGravity = gravity;
		isJumping = false;
		readyToShoot = true;
		maxAmmoCount = 1;
		ammoCount = maxAmmoCount;
	}

	void HandleInput();
	void GravityEffect(float p_gravity);
	void Update() override;
	void SetGravity(float newGravity);
	float GetGravity();
	bool isGrounded;
	bool isJumping;
	std::vector<std::shared_ptr<Entity_Projectile>> CreateProjectile();
	std::vector<std::shared_ptr<Entity_Projectile>> currentProjectiles;
	SDL_Texture* myBulletTexture;

private:
	void HandleKeyDown(SDL_Event *event);
	void HandleKeyUp(SDL_Event *event);
	float gravity;
	float currentGravity;
	float jumpStrength;
	float maxJumpHeight;
	float jumpStartHeight;
	bool readyToShoot;
	int ammoCount;
	int maxAmmoCount;
};