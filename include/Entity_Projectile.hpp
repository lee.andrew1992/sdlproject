#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "Math.hpp"
#include "PhysicsData.hpp"
#include "Entity.hpp"
#include "Math.hpp"

class Entity_Projectile: public Entity
{
public:
	Entity_Projectile(Vector2f p_v, SDL_Texture* p_tex, SDL_Event* p_event, float width, float height): Entity(p_v, p_tex, p_event, width, height)
	{}

	void Update() override;
};