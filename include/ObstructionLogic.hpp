#pragma once

#include "Entity.hpp"
#include "RenderWindow.hpp"
#include "Entity_Enemy.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <memory>
#include <iostream>
#include <vector>

class ObstructionLogic
{
public:
	static std::shared_ptr<Entity> CreateGround(Vector2f position, Vector2f size, SDL_Event* event, RenderWindow window);
	template <typename EntityType> static int CheckObstructionsToDelete(std::vector<std::shared_ptr<EntityType>> obstructionEntities)
	{
		int deleteUpTo = -1;

		for(size_t index = 0; index < obstructionEntities.size(); index++)
		{
			if (obstructionEntities[index]->position.x + obstructionEntities[index]->physicsData.width < 0)
			{
				deleteUpTo++;
			}
		}

		return deleteUpTo;
	};
	template <typename EntityType> static void DeleteObstructionsTo(std::vector<std::shared_ptr<EntityType>> &obstructionEntities, int deleteUpTo)
	{
		if (deleteUpTo == 0)
		{
			obstructionEntities.erase(obstructionEntities.begin());
		}
		else
		{
			obstructionEntities.erase(obstructionEntities.begin(), obstructionEntities.begin() + deleteUpTo);	
		}
	};
	static void FillUpGround(std::vector<std::shared_ptr<Entity>> &groundEntities, SDL_Event* event, RenderWindow window);
	static void FillUpEnemies(std::vector<std::shared_ptr<Entity_Enemy>> &enemyEntities, SDL_Event* event, RenderWindow window);
	static std::shared_ptr<Entity_Enemy> CreateEnemy(Vector2f position, Vector2f size, SDL_Event* event, RenderWindow window);

private:
	ObstructionLogic(){}
	static constexpr float randomGroundHeightMultiplierMax = 0.4;
	static constexpr float randomGroundHeightMultiplierMin = 0.1;
	static constexpr float randomGroundWidthMultiplierMax = 0.4;
	static constexpr float randomGroundWidthMultiplierMin = 0.12;
};
