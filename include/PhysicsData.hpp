#pragma once

#include <iostream>

class PhysicsData
{
	public:
	PhysicsData()
	:width(0.0f), height(0.0f)
	{}

	PhysicsData(float p_w, float p_h)
	:width(p_w), height(p_h)
	{}

	void print()
	{
		std::cout << width << ", " << height << std::endl;
	}

	float width, height;
};