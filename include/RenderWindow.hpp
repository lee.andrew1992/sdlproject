#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <memory>
#include "Entity.hpp"
#include "Entity_Projectile.hpp"

class RenderWindow
{
public:
	RenderWindow(const char* p_title, int p_w, int p_h);
	SDL_Texture* LoadTexture(const char* p_filePath);
	void CleanUp();
	void Clear();
	template <typename EntityType> void Render(std::shared_ptr<EntityType> entity)
	{
		SDL_Rect src;
		src.x = 0;
		src.y = 0;
		src.w = entity->physicsData.width;
		src.h = entity->physicsData.height;

		SDL_Rect dest;
		dest.x = entity->position.x;
		dest.y = entity->position.y;
		dest.w = entity->physicsData.width;
		dest.h = entity->physicsData.height;

		SDL_RenderCopy(renderer, entity->tex, &src, &dest);
	};
	void Display();
	int screenWidth;
	int screenHeight;
 
private:
	SDL_Window* window;
	SDL_Renderer* renderer;
};