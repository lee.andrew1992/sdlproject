#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <vector>
#include <iostream>
#include <memory>
#include "Entity.hpp"
#include "Entity_Player.hpp"
#include "Math.hpp"
#include "RenderWindow.hpp"
#include "AssetManager.hpp"

class Utils
{
public:
	static std::string GetKeyInputName(SDL_KeyboardEvent key);
	static void CreateCharacter(std::vector<std::shared_ptr<Entity_Player>> &characterContainer, Vector2f startingPosition, SDL_Texture* characterTexture, SDL_Event* event);

private:
	Utils(){}
};