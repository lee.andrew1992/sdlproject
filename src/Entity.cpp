#include "Entity.hpp"
#include "Math.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

Entity::Entity(Vector2f p_v, SDL_Texture* p_tex, SDL_Event* p_event, float width, float height)
{
	position = p_v;
	tex = p_tex;
	event = p_event;

	physicsData = PhysicsData(width, height);
}

void Entity::MoveEntity(Vector2f p_dir)
{
	position.x += p_dir.x;
	position.y += p_dir.y;
}

void Entity::Update()
{
	MoveEntity(Vector2f(-0.65, 0));
}