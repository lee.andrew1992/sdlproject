#include "Entity_Enemy.hpp"
#include "Math.hpp"
#include "Utils.hpp"
#include <SDL2/SDL.h>
#include <iostream>
#include <math.h>
#include <algorithm>

void Entity_Enemy::Update()
{
	float speed = 0;
	float sizeRatio = physicsData.width / screenWidth;
	if (sizeRatio > 0.05)
	{
		speed = 0.35;
	}
	else
	{
		speed = 1 - sizeRatio * 10;
	}

	MoveEntity(Vector2f(-speed, 0));
}