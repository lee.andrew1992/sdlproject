#include "Entity_PhysicsLogic.hpp"
#include "Entity.hpp"
#include "Entity_Player.hpp"
#include "Math.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <vector>
#include <iostream>
#include <memory>

bool Entity_PhysicsLogic::GroundCheck(std::vector<std::shared_ptr<Entity>> groundEntities, std::shared_ptr<Entity_Player> player)
{
	if (player->GetGravity() < 0)
	{
		return false;
	}

	for (size_t index = 0; index < 2; index++)
	{
		if ((player->position.x >= groundEntities[index]->position.x && player->position.x < groundEntities[index]->position.x + groundEntities[index]->physicsData.width) ||
			(player->position.x + player->physicsData.width >= groundEntities[index]->position.x && player->position.x + player->physicsData.width < groundEntities[index]->position.x + groundEntities[index]->physicsData.width))
		{
			if (player->position.y + player->physicsData.height >= groundEntities[index]->position.y &&
				player->position.y + player->physicsData.height < groundEntities[index]->position.y + groundEntities[index]->physicsData.height)
			{
				player->SetGravity(0);
				player->isGrounded = true;
				return true;
			}
		}
	}
	
	player->SetGravity(1);
	player->isGrounded = false;
	return NULL;
}

void Entity_PhysicsLogic::ProjectileHitCheck(std::vector<std::shared_ptr<Entity_Enemy>> &enemyEntities, std::vector<std::shared_ptr<Entity_Projectile>> &projectiles)
{
	for(size_t p_index = 0; p_index < projectiles.size(); p_index++)
	{
		for (size_t e_index = 0; e_index < enemyEntities.size(); e_index++)
		{
			if (EnemyCheck(enemyEntities[e_index], projectiles[p_index]))
			{
				enemyEntities.erase(enemyEntities.begin() + e_index);
				projectiles.erase(projectiles.begin() + p_index);

				break;
			}
		}
	}
}

bool Entity_PhysicsLogic::DeathCheck(float windowHeight, std::vector<std::shared_ptr<Entity_Enemy>> enemyEntities, std::shared_ptr<Entity_Player> player)
{
	for(size_t index = 0; index < enemyEntities.size(); index++)
	{
		if (EnemyCheck(enemyEntities[index], player))
		{
			return true;
		}
	}

	if (player->position.y > windowHeight)
	{
		return true;
	}
	return false;
}