#include "Entity_Player.hpp"
#include "Entity_Projectile.hpp"
#include "Math.hpp"
#include "Utils.hpp"
#include <SDL2/SDL.h>
#include <iostream>
#include <math.h>
#include <algorithm>

void Entity_Player::HandleInput()
{
	switch(event->type)
	{
		case SDL_KEYDOWN:
			HandleKeyDown(event);
			break;

		case SDL_KEYUP:
		{
			HandleKeyUp(event);
			break;
		}
	}
}

void Entity_Player::Update()
{
	MoveEntity(Vector2f(0, currentGravity));
	if (isJumping)
	{
		if (jumpStartHeight - position.y > maxJumpHeight)
		{
			SetGravity(std::min(currentGravity + (gravity * 0.1), (double)gravity));
		}
	}

	if (isGrounded)
	{
		isJumping = false;
	}

	HandleInput();
}

void Entity_Player::HandleKeyDown(SDL_Event *event)
{
	std::string keyPressed = Utils::GetKeyInputName(event->key);

	if (keyPressed == "Left Ctrl")
	{
		if (isGrounded)
		{
			// Do jump
			SetGravity(-jumpStrength);
			isJumping = true;
			isGrounded = false;
			jumpStartHeight = position.y;
			if (ammoCount < maxAmmoCount)
			{
				ammoCount += 1;
			}
		}
	}
	if (keyPressed == "Left Alt")
	{
		currentProjectiles = CreateProjectile();
	}
}

void Entity_Player::HandleKeyUp(SDL_Event *event)
{
	std::string keyLetGo = Utils::GetKeyInputName(event->key);
	if (keyLetGo == "Left Ctrl")
	{
		isJumping = false;
		SetGravity(gravity);
	}
	if (keyLetGo == "Left Alt")
	{
		readyToShoot = true;
	}
}

void Entity_Player::SetGravity(float newGravity = NAN)
{
	if (isnan(newGravity))
	{
		currentGravity = gravity;
	}
	else
	{
		currentGravity = newGravity;
	}
}

float Entity_Player::GetGravity()
{	
	return currentGravity;
}

std::vector<std::shared_ptr<Entity_Projectile>> Entity_Player::CreateProjectile()
{
	if (readyToShoot && ammoCount > 0)
	{
		ammoCount -= 1;
		readyToShoot = false;

		auto projectile = std::make_shared<Entity_Projectile>(position, myBulletTexture, event, 50, 15);
		projectile->position.x = position.x;
		projectile->position.y = position.y;

		currentProjectiles.push_back(projectile);
	}

	return currentProjectiles;
}