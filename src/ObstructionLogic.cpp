#include "Entity.hpp"
#include "Math.hpp"
#include "RenderWindow.hpp"
#include "ObstructionLogic.hpp"
#include "AssetManager.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <vector>
#include <memory>
#include <cstdlib>
#include <cmath>

std::shared_ptr<Entity> ObstructionLogic::CreateGround(Vector2f position, Vector2f size, SDL_Event* event, RenderWindow window)
{
	SDL_Texture* groundTexture = AssetManager::GetGroundTexture(window);
	return std::make_shared<Entity>(position, groundTexture, event, size.x, size.y);
}

void ObstructionLogic::FillUpEnemies(std::vector<std::shared_ptr<Entity_Enemy>> &enemyEntities, SDL_Event* event, RenderWindow window)
{
	float enemyHeightMultiplier = (rand() % 40 + 15);
	float enemyHeight = (enemyHeightMultiplier * window.screenHeight) / 100;
	float enemySize = (rand() % (window.screenWidth) + (window.screenWidth) * 1.5) / 100;

	if (enemyEntities.size() < 4)
	{
		if (enemyEntities.size() == 0 || enemyEntities[enemyEntities.size() - 1]->position.x < window.screenWidth)
		{
			auto newEnemy = CreateEnemy(Vector2f(window.screenWidth * 1.1, enemyHeight), Vector2f(enemySize, enemySize), event, window);
			enemyEntities.push_back(newEnemy);
		}
		else 
		{	
			float enemyGap = rand() % 500 + 100;

			auto newEnemy = CreateEnemy(Vector2f(enemyEntities[enemyEntities.size() - 1]->position.x + enemyGap, enemyHeight), Vector2f(enemySize, enemySize), event, window);
			enemyEntities.push_back(newEnemy);
		}
	}
}

void ObstructionLogic::FillUpGround(std::vector<std::shared_ptr<Entity>> &groundEntities, SDL_Event* event, RenderWindow window)
{
	if (groundEntities.size() < 6)
	{
		float groundHeightMultiplier = (rand() % (int)(100 * randomGroundHeightMultiplierMax) + (int)(100 * randomGroundHeightMultiplierMin)) * 0.01;
		float groundWidthMultiplier = (rand() % (int)(100 * randomGroundWidthMultiplierMax) + (int)(100 * randomGroundWidthMultiplierMin)) * 0.01;
		float groundGap = rand() % 100 + 50;

		auto closestGround = groundEntities[groundEntities.size() - 1];
		auto newGroundHeight = window.screenHeight - (window.screenHeight * groundHeightMultiplier);
		auto heightGap = std::abs(closestGround->position.y - newGroundHeight);

		if (heightGap > window.screenHeight * 0.15)
		{
			newGroundHeight = (newGroundHeight + closestGround->position.y) / 2;
		}

		auto newGround = CreateGround(Vector2f(closestGround->position.x + closestGround->physicsData.width + groundGap, newGroundHeight), Vector2f(window.screenWidth * groundWidthMultiplier, window.screenHeight * 0.025), event, window);
		groundEntities.push_back(newGround);
	}
}

std::shared_ptr<Entity_Enemy> ObstructionLogic::CreateEnemy(Vector2f position, Vector2f size, SDL_Event* event, RenderWindow window)
{
	SDL_Texture* enemyTexture = AssetManager::GetEnemyTexture(window);
	auto enemy = std::make_shared<Entity_Enemy>(position, enemyTexture, event, size.x, size.y, window);

	return enemy;
}