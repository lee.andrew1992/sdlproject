#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <vector>
#include <iostream>
#include <memory>
#include "Entity.hpp"
#include "Entity_Player.hpp"
#include "Math.hpp"
#include "RenderWindow.hpp"
#include "AssetManager.hpp"
#include "Utils.hpp"

std::string Utils::GetKeyInputName(SDL_KeyboardEvent key)
{
    return SDL_GetKeyName(key.keysym.sym);
}

void Utils::CreateCharacter(std::vector<std::shared_ptr<Entity_Player>> &characterContainer,  Vector2f startingPosition, SDL_Texture* characterTexture, SDL_Event* event)
{
	auto character = std::make_shared<Entity_Player>(startingPosition, characterTexture, event, 32, 32);
	character->myBulletTexture = characterTexture;
	characterContainer.push_back(character);	
}
