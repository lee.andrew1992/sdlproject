#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <vector>
#include "RenderWindow.hpp"
#include "Entity.hpp"
#include "Entity_Player.hpp"
#include "Entity_Enemy.hpp"
#include "Entity_Projectile.hpp"
#include "Entity_PhysicsLogic.hpp"
#include "Math.hpp"
#include "ObstructionLogic.hpp"
#include "Utils.hpp"
#include "AssetManager.hpp"

int main(int argc, char* args[])
{
	if (SDL_Init(SDL_INIT_VIDEO) > 0)
	{
		std::cout << "SDL_Init HAS FAILED. SDL_ERROR: " << SDL_GetError() << std::endl;
	}

	if (!(IMG_Init(IMG_INIT_PNG)))
	{
		std::cout << "IMG_Init has FAILED. Error: " << SDL_GetError() << std::endl;
	}

	int screenWidth = 1280;
	int screenHeight = 720;

	RenderWindow window("GAME v1.0", screenWidth, screenHeight);

	SDL_Event event;
	SDL_Texture* characterTexture = AssetManager::GetCharacterTexture(window);

	std::vector<std::shared_ptr<Entity_Player>> characterContainer;
	Utils::CreateCharacter(characterContainer, Vector2f(screenWidth / 5, 0), characterTexture, &event);
	auto character = characterContainer[0];

	std::vector<std::shared_ptr<Entity>> groundEntities;
	std::vector<std::shared_ptr<Entity_Enemy>> enemyEntities;

	// Create Initial Large Ground
	auto initialGround = ObstructionLogic::CreateGround(Vector2f(0, screenHeight - (screenHeight / 3.5)), Vector2f(screenWidth * 0.9, screenHeight - 150), &event, window);
	groundEntities.push_back(initialGround);

	bool gameRunning = true;

	while(gameRunning)
	{
		if (!Entity_PhysicsLogic::DeathCheck(screenHeight, enemyEntities, character))
		{
			while(SDL_PollEvent(&event))
			{
				if (event.type == SDL_QUIT)
				{
					gameRunning = false;
				}
			}

			window.Clear();
			int deleteGroundsUpTo = ObstructionLogic::CheckObstructionsToDelete(groundEntities);

			if (deleteGroundsUpTo >= 0)
			{
				ObstructionLogic::DeleteObstructionsTo(groundEntities, deleteGroundsUpTo);
			}

			int deleteEnemiesUpTo = ObstructionLogic::CheckObstructionsToDelete(enemyEntities);
			if (deleteEnemiesUpTo >= 0)
			{
				ObstructionLogic::DeleteObstructionsTo(enemyEntities, deleteEnemiesUpTo);
			}

			// Fill up moving ground
			ObstructionLogic::FillUpGround(groundEntities, &event, window);
			ObstructionLogic::FillUpEnemies(enemyEntities, &event, window);

			character->Update();
			for (size_t index = 0; index < groundEntities.size(); index++)
			{
				groundEntities[index]->Update();
				if (groundEntities[index]->position.x < screenWidth)
				{
					window.Render(groundEntities[index]);
				}
			}

			for (size_t index = 0; index < enemyEntities.size(); index++)
			{
				enemyEntities[index]->Update();
				if (enemyEntities[index]->position.x < screenWidth)
				{
					window.Render(enemyEntities[index]);
				}
			}

			for (size_t index = 0; index < character->currentProjectiles.size(); index++)
			{
				character->currentProjectiles[index]->Update();

				if (character->currentProjectiles[index]->position.x < screenWidth)
				{
					window.Render(character->currentProjectiles[index]);
				}
			}

			Entity_PhysicsLogic::GroundCheck(groundEntities, character);
			Entity_PhysicsLogic::ProjectileHitCheck(enemyEntities, character->currentProjectiles);
		}

		else
		{
			ObstructionLogic::DeleteObstructionsTo(groundEntities, groundEntities.size());
			ObstructionLogic::DeleteObstructionsTo(enemyEntities, enemyEntities.size());
			characterContainer.erase(characterContainer.begin());

			auto newGround = ObstructionLogic::CreateGround(Vector2f(0, screenHeight - (screenHeight / 3.5)), Vector2f(screenWidth * 0.9, screenHeight - 150), &event, window);
			groundEntities.push_back(newGround);

			Utils::CreateCharacter(characterContainer, Vector2f(screenWidth / 5, 0), characterTexture, &event);
			character = characterContainer[0];
		}

		window.Render(character);
		window.Display();
	}

	window.CleanUp();
	SDL_Quit();

	return 0;
}